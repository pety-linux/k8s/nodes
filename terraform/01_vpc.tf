resource "aws_vpc" "vpc" {
  cidr_block                       = var.aws_vpc_cidr
  instance_tenancy                 = "default"
  enable_dns_hostnames             = true
  enable_dns_support               = true
  assign_generated_ipv6_cidr_block = false

  tags = {
    Name = "${var.project_name}VPC"
  }
}
