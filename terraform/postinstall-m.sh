#!/bin/bash

# Create User
useradd -s /bin/bash -c "Student" -m student
echo "Passw0rd" | passwd --stdin student
# Set sudo
echo "student ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers

# Adjust SSH for using passwords
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
systemctl restart sshd
##

## Install K8S
yum -y install git jq
# Clone Repo
git clone https://gitlab.com/pety-linux/k8s/k8s-install.git
# Run K8S Installation Script
bash k8s-install/install-node-containerd.sh
##

## Install Helm
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
##

## Add aliases
cat <<EOF | tee -a /etc/bashrc
# Aliases
alias k=kubectl
alias c=clear
EOF
##
